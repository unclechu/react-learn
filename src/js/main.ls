/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Viacheslav Lotsmanov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

require! {
  \prelude-ls : { camelize, map, sort-by, fold, last, flip, filter, any }
  \./react.ls : {
    React: { DOM: d, Component, create-element, PropTypes }
    ReactDOM
  }
}

class SearchField extends Component
  @default-props =
    {} <<< super.default-props <<< do
      just-button-name: 'just button'
  @prop-types =
    just-button-name : PropTypes.string.is-required
    filter-text      : PropTypes.string
    in-stock-only    : PropTypes.bool
    on-user-input    : PropTypes.func
  (props)!~>
    super props
    @state =
      counter: 0
      timer-id: null
  handle-change: !~>
    @props.on-user-input do
      @refs.filter-text-input.value
      @refs.in-stock-only-input.checked
  @interval = 1000
  start-counter: !~>
    return if @state.counter > 0
    @set-state counter: 5
    do @debounce-dec-counter
  debounce-dec-counter: !~>
    (flip set-timeout) @@interval, !~>
      @set-state counter: (- 1) @state.counter
      do @debounce-dec-counter unless @state.counter < 1
  render: ~>
    d.div class-name: 'input-group search-field',
      d.div class-name: \input-group-btn,
        d.button do
          do
            class-name : 'btn btn-success'
            on-click   : @start-counter
          if @state.counter < 1 then \Start! else @state.counter
        d.button class-name: 'btn btn-success',
          @props.just-button-name
      d.input do
        class-name  : \form-control
        type        : \text
        placeholder : \Search...
        value       : @props.filter-text
        ref         : camelize \filter-text-input
        on-change   : @handle-change
      d.label class-name: \input-group-addon,
        d.input do
          type       : \checkbox
          checked    : @props.in-stock-only
          ref        : camelize \in-stock-only-input
          on-change  : @handle-change
        ' Only show products in stock'

class ProductCategoryTitle extends Component
  @prop-types =
    title: PropTypes.string.is-required
  render: ~>
    d.tr class-name: \product-category-title,
      d.th col-span: 2, @props.title

class ProductItem extends Component
  @prop-types =
    name: PropTypes.string.is-required
    price:
      (.is-required) PropTypes.one-of-type do
        [ PropTypes.string, PropTypes.number ]
    is-stocked: PropTypes.bool.is-required
  render: ~>
    d.tr class-name: "
                     product-item
                     \ #{if @props.is-stocked then \product-in-stock else ''}
                     ",
      d.td null, @props.name
      d.td null, @props.price

class ProductsList extends Component
  @prop-types =
    in-stock-only: PropTypes.bool
    filter-text: PropTypes.string
    products: PropTypes.array-of PropTypes.shape do
      category: PropTypes.string.is-required
      name: PropTypes.string.is-required
      price:
        (.is-required) PropTypes.one-of-type do
          [ PropTypes.string, PropTypes.number ]
      is-stocked: PropTypes.bool.is-required
  in-stock-filter: ~>
    | @props.in-stock-only => it.is-stocked
    | otherwise => on
  text-filter: (item)~>
    text = @props.filter-text.to-lower-case!.trim!
    switch
    | text isnt '' =>
        <[ name category ]>
           |> map (-> item[it].to-lower-case!.trim!)
           |> any (-> (>= 0) it.index-of text)
    | otherwise => on
  render: ~>
    d.div class-name: 'panel panel-default products-list',
      d.div class-name: \panel-heading, 'Products list'
      d.table class-name: \table,
        d.thead null,
          d.tr null,
            d.th null, \Name
            d.th null, \Price
        d.tbody null,
          @props.products
            |> sort-by (.category)
            |> filter @in-stock-filter
            |> filter @text-filter
            |> (flip fold) [null, []], (tuple, item) ->
                 [ last-category, list ] = tuple
                 if last-category isnt item.category
                   tuple.0 = item.category
                   list.push <|
                     create-element ProductCategoryTitle, do
                       key   : item.category
                       title : item.category
                 list.push <|
                   create-element ProductItem, do
                     { key: item.name } <<< item
                 tuple
            |> (.1)

class Main extends Component
  @prop-types =
    products: ProductsList.prop-types.products
  (props)!~>
    super props
    @state =
      filter-text   : ''
      in-stock-only : no
  handle-user-input: (filter-text, in-stock-only)!~>
    @set-state do
      filter-text   : filter-text
      in-stock-only : in-stock-only
  render: ~>
    d.div null,
      create-element SearchField, do
        filter-text   : @state.filter-text
        in-stock-only : @state.in-stock-only
        on-user-input : @handle-user-input
      create-element ProductsList, do
        products      : @props.products
        filter-text   : @state.filter-text
        in-stock-only : @state.in-stock-only

products =
  * category: \Sport,       name: 'Tennis ball',   price: \5$,  is-stocked: no
  * category: \Sport,       name: 'Tennis racket', price: \8$,  is-stocked: yes
  * category: \Electronics, name: 'Computer',      price: \40$, is-stocked: yes
  * category: \Sport,       name: 'Whip',          price: \3$,  is-stocked: yes
  * category: \Electronics, name: 'CD',            price: \1$,  is-stocked: no

ReactDOM.render do
  create-element Main, { products }
  document.query-selector \#main
